from PySide.QtCore import QThread, QEvent
from DModule.drawingModel import DrawingModel
import time
from numpy import ndarray
from .pfinder_instance import pf


class CustomThread(QThread):
	_work = 0

	def start(self):
		self._work = 1
		super(CustomThread, self).start()

	def off(self, forced=False):
		self._work = 0
		if not forced:
			self.wait()

	def switch(self):
		if self._work:
			self.off()
		else:
			self.on()

	def _loop(self):
		pass

	def _end(self):
		pass

	def _start(self):
		pass

	def run(self, *args, **kwargs):
		self._start()
		while self._work:
			time.sleep(0.01)  # TODO FIX IT
			self._loop()
		self._end()


DATA_SIZE = 150
PTS = 70


class PointsReceiveThread(CustomThread):
	def __init__(self, widget, socket, from_addr, user):
		super(PointsReceiveThread, self).__init__()
		self._socket = socket
		self._widget = widget
		self._from_addr = from_addr

		self.user = user

		self.dmodel = DrawingModel(1, 1)
		self.addresses = set()

	def _loop(self):
		data, addr = self._socket.recvfrom(DATA_SIZE)
		if addr != self._from_addr:
			if addr not in self.addresses:
				usr = self.user.get_contact_by_addr(addr)
				if usr:
					qevent = QEvent(QEvent.Type(555))
					qevent.uid = usr[0]

					self.user.qtapp.postEvent(self.user.main_window.in_calls_layout, qevent)
					self.addresses.add(addr)
			return
		polygons = self.handle_data(data)
		if polygons:
			self._widget._polygons = polygons
			self._widget.update()

	def handle_data(self, data):
		# data = list(data)
		pts = ndarray(shape=(300, 2))
		i = 0
		while i < PTS:
			pts[i, 0] = data[i * 2]
			pts[i, 1] = data[i * 2 + 1]
			i += 1
		return self.dmodel.process(pts)

	def set_from_addr(self, from_addr):
		self._from_addr = from_addr

	def stop_call(self):
		self.set_from_addr(("", -1))
		self.user.qtapp.postEvent(self._widget, QEvent(QEvent.Type(999)))


class PointsSendThread(CustomThread):
	def __init__(self, socket):
		super(PointsSendThread, self).__init__()
		self._socket = socket

		self.points_finder = pf
		self.to_addr = ()

	def set_to_addr(self, to_addr):
		self.to_addr = to_addr

	@staticmethod
	def to255(a):
		return 255 if a > 255 else a

	def _loop(self):
		pts = self.points_finder.get_points()
		if len(pts) == 0:
			return
		data = bytearray(PTS * 2)
		for i in range(PTS):
			data[i * 2] = self.to255(pts[i][0])
			data[i * 2 + 1] = self.to255(pts[i][1])
		self._socket.sendto(data, self.to_addr)

	def close(self):
		self.points_finder.release_cam()

	def _end(self):
		self.close()

	def _start(self):
		self.points_finder.open_cam()


#
#  TODO threads singletons

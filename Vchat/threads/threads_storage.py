from .threads import CustomThread, PointsSendThread


class Threads:
	recv_pts_thread = CustomThread()
	send_pts_thread = CustomThread()

	@staticmethod
	def set_recv_pts_thread(thread):
		Threads.recv_pts_thread = thread

	@staticmethod
	def set_send_pts_thread(thread):
		Threads.send_pts_thread = thread

	@staticmethod
	def stop_threads():
		Threads.recv_pts_thread.off()
		Threads.send_pts_thread.off()



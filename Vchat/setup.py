import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning. ,
build_exe_options = {
	"include_files": [
		"superdata", "client", "GUI", "GLSL", "resources", "threads", "utils",
		"not_release", "data", "DModule", "DScenes", ""],
	"packages": ["json", "numpy", "cv2", "dlib", "PySide", "imutils", "OpenGL"],
	"build_exe": "../client_build"}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
	base = "Win32GUI"

setup(
	name="vchat",
	version="0.1",
	description="My GUI application!",
	options={"build_exe": build_exe_options},
	executables=[Executable("authorization.py", base=base)])

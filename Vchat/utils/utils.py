def read_all(filename):
	string = ""
	with open(filename) as f:
		for line in f.readlines():
			string += line
	return string

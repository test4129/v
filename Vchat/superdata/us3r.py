from client.mainclient import MainClient
from superdata.us3rData import UserData
from client.p2psimple import P2PManager
from GUI.authorizationWindow import AuthorizationWindow
import sys
from utils.utils import read_all
from PySide.QtGui import QApplication
from hashlib import sha256
from os.path import isfile
import client.codes as codes


class User:
	def __init__(self):
		self.user_data = UserData()

		self.password = ""
		self.login = ""
		self.load_udata()

		self.id = -1

		self.pconn_manager = P2PManager(self)
		self.server = MainClient(self, in_port=self.pconn_manager.get_in_port())
		self.qtapp = object()
		self.main_window = object()

	def set_main_window(self, main_window):
		self.main_window = main_window

	def get_contacts(self):
		return self.user_data.contacts.values()

	def get_contact(self, uid):
		if uid not in self.user_data.contacts:
			return False
		return self.user_data.contacts[uid]

	def get_contact_by_addr(self, addr):
		for contact in self.user_data.contacts.values():
			if contact[2] == addr:
				return contact
		return False

	def set_contacts_from_list(self, contacts):
		self.user_data.contacts = {}
		for contact in contacts:
			self.user_data.contacts[contact[0]] = contact[0], contact[1], (contact[2], contact[3])

	def set_requests(self, requests):
		self.user_data.requests = requests

	def get_requests(self):
		return self.user_data.requests

	def set_user_data(self, login, password):
		self.login = login
		self.password = sha256(password.encode()).hexdigest()

	@staticmethod
	def _is_valid(login, password = ""):
		return True

	def log_in(self):
		app = QApplication(sys.argv)
		app.setStyleSheet(
			read_all("resources/style.css") +
			read_all("resources/style_contacts.css") +
			read_all("resources/style_buttons.css") +
			read_all("resources/style_calls.css")
		)
		self.qtapp = app
		if self.log_in_() == codes.LOGGED_IN:
			return codes.LOGGED_IN, app
		authorizationWindow = AuthorizationWindow(self)
		authorizationWindow.move(500, 500)
		authorizationWindow.show()
		app.exec_()
		return authorizationWindow.get_code(), app

	def log_in_(self, save=False):
		if self.login == "" or self.password == "":
			return codes.NO_DATA
		result = self.server.log_in(self.login, self.password)
		if result == codes.LOGGED_IN and save:
			self.save_udata()
		return result

	def call(self, uid):
		user.pconn_manager.start_receiving(uid)
		user.pconn_manager.start_sending(uid)

	def stop_call(self):
		self.main_window.right.canvas.hideIt()
		self.pconn_manager.stop_sending()

	def refuse(self, uid=-1):
		pass

	def load_udata(self):
		if isfile("udata"):
			try:
				with open("udata", "r") as f:
					self.login = f.readline().strip()
					self.password = f.readline().strip()
					return
			except IOError:
				pass
		self.login = ""
		self.password = ""

	def save_udata(self):
		try:
			with open("udata", "w") as f:
				f.writelines([self.login, "\n", self.password])
		except IOError:
			pass


user = User()

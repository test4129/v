class UserData:
	def __init__(self):
		# Contact MODEL    UID, Login, (ip, port)
		self.contacts = {
			48951:   (48951,   "Tommy",    ("127.0.0.1", 7777)),
			456:     (456,     "Vladimir", ("127.0.0.1", 6777)),
			78979:   (78979,   "Gumball",  ("127.0.0.1", 7737)),
			2177635: (2177635, "R0B07",    ("127.0.0.1", 7727)),
			522:     (522,     "N0N4M3",   ("127.0.0.1", 7477)),
		}
		# Request MODEL    UID, Login
		self.requests = [
		]

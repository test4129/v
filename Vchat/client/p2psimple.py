from threading import Thread
from threads.threads import PointsSendThread, PointsReceiveThread
from threads.threads_storage import Threads
from socket import socket, AF_INET, SOCK_DGRAM


class P2PManager(Thread):
	def __init__(self, user):
		super(P2PManager, self).__init__()
		self.user = user
		self.pts_socket = socket(AF_INET, SOCK_DGRAM)
		self.pts_socket.bind(("localhost", 0))

		self.in_conn = False
		self.out_conn = False
		self.from_addr = ()

		Threads.set_send_pts_thread(PointsSendThread(self.pts_socket))

	def get_in_port(self):
		return self.get_in_pts_port()

	def get_in_pts_port(self):
		return self.pts_socket.getsockname()[1]

	def make_pts_receive_thread(self, widget):
		return PointsReceiveThread(socket=self.pts_socket, widget=widget, from_addr=self.from_addr, user=self.user)

	def connected(self):
		return self.in_conn and self.out_conn

	def start_receiving(self, uid):
		usr = self.user.get_contact(uid)
		if usr and usr[2][1] != -1:
			self.in_conn = True
			self.from_addr = usr[2]

	def start_sending(self, uid):
		usr = self.user.get_contact(uid)
		if usr and usr[2][1] != -1:
			self.out_conn = True
			self.stop_sending()
			Threads.send_pts_thread.set_to_addr(usr[2])
			Threads.send_pts_thread.start()

	def stop_sending(self):
		Threads.send_pts_thread.close()
		Threads.send_pts_thread.terminate()

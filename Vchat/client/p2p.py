from socket import socket, AF_INET, SOCK_DGRAM
from threads import PointsReceiveThread, PointsSendThread
from superdata.us3r import user
import threading

#   P2P DATA MODEL
#   18 bytes
#   ( @ #### PP VV )x2 where @ - command, #### - user id, PP - pts socket port, VV - voice socket port

PACKET_SIZE = 18


class Commands:
	CALL = 0
	STOP = 1
	ACCEPT = 2
	REFUSE = 3


class PConnManager:
	class InHandler(threading.Thread):
		def __init__(self, sock, owner):
			super(PConnManager.InHandler, self).__init__()
			self._sock = sock
			self._p2p_manager = owner

		def run(self):
			while True:
				data, addr = self._sock.recvfrom(PACKET_SIZE)

				if data[:-len(data) // 2] != data[:-len(data) // 2]:   # check packet are not broken
					print("broken packet")
					return

				data = data[:-len(data)]
				command, uid, pts_port, voice_port = PConnManager.unpack(data)

				if not self.valid_friend(uid, addr):
					return

				if command == Commands.CALL:
					if self._p2p_manager.user_accept_call(self._p2p_manager.is_connected):  # TODO check connected and check calling
						if self._p2p_manager.is_connected:
							self._p2p_manager.send(Commands.STOP, addr)
						self._p2p_manager.set_connected(uid, (addr, pts_port))
						self._p2p_manager.send(Commands.ACCEPT, addr, self._p2p_manager.get_in_pts_port())
					else:
						self._p2p_manager.send(Commands.REFUSE, addr)
				elif command == Commands.ACCEPT:
					if self._p2p_manager.is_calling and self._p2p_manager.connected_addr == addr:
						self._p2p_manager.set_connected(uid, addr, (addr[0], pts_port))
				elif command == Commands.REFUSE:
					self._p2p_manager.stop_calling(True)
				elif command == Commands.STOP:
					self._p2p_manager.disconnect()

		@staticmethod
		def valid_friend(uid, addr):
			curr_user = user.get_contact(uid, addr)
			return curr_user and curr_user[2] == addr

		def valid_accept(self, uid, addr):
			return uid == self._p2p_manager.called_uid == uid and self._p2p_manager.called_addr == addr

	def __init__(self):
		self.manager_socket = socket(AF_INET, SOCK_DGRAM)
		self.manager_socket.bind(("localhost", 0))
		self.pts_socket = socket(AF_INET, SOCK_DGRAM)
		self.pts_socket.bind(("localhost", 0))

		self.pts_send_thread = PointsSendThread(self.pts_socket)

		self.is_calling = False
		self.is_connected = False

		self.connected_uid = -1
		self.called_uid = -1
		self.connected_pts_addr = ()
		self.connected_addr = ()
		self.called_addr = ()

		self.in_handler = PConnManager.InHandler(self.manager_socket, self)

	def get_in_port(self):
		return self.manager_socket.getsockname()[1]

	def get_in_pts_port(self):
		return self.pts_socket.getsockname()[1]

	def make_pts_receive_thread(self, widget):
		return PointsReceiveThread(socket=self.pts_socket, widget=widget, from_addr=self.connected_pts_addr)

	def call(self, uid):
		if not (self.is_connected or self.is_calling):
			another_user = user.get_contact(uid)
			addr = another_user[2]
			self.start_calling(uid, addr)
			self.send(Commands.CALL, addr, self.get_in_pts_port())
		else:
			print("\\\\&@!*&)JFP@!(*$!^*@#//")

	def stop_call(self):
		self.stop_calling(True)

	def send(self, command, addr, pts_port=0, voice_port=0):
		self.manager_socket.sendto(self.pack(command, user.id, pts_port, voice_port), addr)

	def set_connected(self, uid, addr, paddr):
		self.is_calling = False
		self.is_connected = True
		self.connected_uid = uid
		self.connected_addr = addr
		self.connected_pts_addr = paddr

	def disconnect(self):
		self.is_connected = False

	def kill_connection(self):
		self.is_connected = False
		self.send(Commands.STOP, self.connected_addr)

	def stop_calling(self, refused):
		print(("you stopped call, idiot", "your friend is dickhead")[refused])
		self.is_calling = False

	def start_calling(self, uid, addr):
		self.is_calling = True
		self.called_addr = addr

	def user_accept_call(self, is_calling):
		return True

	@staticmethod
	def pack(command, uid, pts_port, voice_port):
		return bytearray(command) + uid.to_bytes(4, "big") + pts_port.to_bytes(2, "big") + voice_port.to_bytes(2, "big")

	@staticmethod
	def unpack(data):
		command = data[0]
		uid = int.from_bytes(data[1:5], "big")
		pts_port = int.from_bytes(data[5:7], "big")
		voice_port = int.from_bytes(data[7:9], "big")
		return command, uid, pts_port, voice_port


import socket
from utils.parsejson import object2bjson, bjson2object, bjson2objects


class NetWorker:
	def __init__(self, address, port):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setblocking(True)
		self.port = port
		self.address = address

	def connect(self):
		self.sock.connect((self.address, self.port))

	def send(self, data):
		req = object2bjson(data)
		self.sock.send(req)

	def receive(self, size=128):
		return bjson2object(self.sock.recv(size))

	def receive_many(self, size=128):
		return bjson2objects(self.sock.recv(size))

	def close(self):
		self.sock.close()

	def detach(self):
		self.sock.detach()


from .netWorker import NetWorker
from .async_net.getThread import GetThread
import settings
import client.codes as codes


class MainClient:
	def __init__(self, user, in_port):
		self.net_worker = NetWorker(address=settings.address, port=settings.mainPort)
		self.user = user
		self.token = ""

		self.in_port = in_port

	def send2server(self, operation, data):
		data["operation"] = operation
		data["token"] = self.token
		data["id"] = self.user.id
		self.net_worker.send(data)

	def remove_friend(self, uid):
		self.send2server("remove_friend", {"uid": uid})

	def send_request(self, uid):
		self.send2server("send_request", {"uid": uid})

	def add_to_contacts(self, uid):
		self.send2server("add_to_contacts", {"uid": uid})

	def search_people(self, q):
		self.send2server("search_people", {"q": q})

	def get_requests(self):
		self.send2server("get_requests", {})

	def log_in(self, login, password):
		self.net_worker = NetWorker(address=settings.address, port=settings.mainPort)
		self.net_worker.connect()
		self.net_worker.send({"operation": "log_in", "data": {
															"login": login,
															"password": password,
															"port": self.in_port}
							})
		res = self.net_worker.receive(8196)
		if res["result"] == "success":
			self.user.id = res["id"]
			self.token = res["token"]
			self.user.set_contacts_from_list([])

			thread = GetThread(self.net_worker, self.user, self.token)
			self.thread = thread

			return codes.LOGGED_IN
		else:
			self.net_worker.close()

		if res["result"] == "invalid":
			return codes.WRONG_DATA

		if res["result"] == "busy":
			return codes.BUSY_USER

		return codes.NO_SERVER_CONNECTION

	def start(self):
		self.thread.start()

	def stop_close(self):
		self.thread.stop()
		self.net_worker.close()


#cimport DModel
#from DModel cimport *
#from shaders cimport *


cdef extern from "math.h":
	cdef double cos(double)

cdef extern from "GL/gl.h":
	ctypedef int GLint
	ctypedef unsigned int GLuint
	ctypedef float GLfloat
	ctypedef unsigned int GLenum
	GLenum GL_POLYGON
	cdef void glBegin(GLenum)
	cdef void glEnd()
	cdef void glFlush()
	cdef void glVertex2f(GLfloat x, GLfloat y)
	cdef void glColor3f(GLfloat, GLfloat, GLfloat)


def drawf(polys):
	glBegin(GL_POLYGON)
	glColor3f(0.184, 0.310, 0.310)
	glVertex2f(1, -0.5)

	glColor3f(0.416, 0.353, 0.804)
	glVertex2f(1, 1.5)
	glVertex2f(-1, 1.5)

	glColor3f(0.184, 0.310, 0.310)
	glVertex2f(-1, -0.5)

	glColor3f(	0.000, 1.000, 0.498)
	glVertex2f(-1, -1.5)
	glVertex2f(1, -1.5)
	glEnd()

	cdef int i = 0
	cdef int xyclen = 0
	for xyc in polys:
		color = xyc[len(xyc) - 1]
		#try:
		glColor3f(color[0], color[1], color[2])
		glBegin(GL_POLYGON)
		i = 0
		xyclen = len(xyc) - 1
		while i < xyclen:
			glVertex2f(xyc[i][0], xyc[i][1])
			i += 1
		glEnd()
		#except(Exception):
		#	print(color)
	glFlush()


from PySide.QtGui import QVBoxLayout
from .panel import Block
from .personImpl import Contact, SearchedPerson, Request
from superdata.us3r import user


class LeftContainer(QVBoxLayout):
	def __init__(self):
		super(LeftContainer, self).__init__()
		self.contacts = Block(Contact, "contacts", user.get_contacts())
		self.requests = Block(Request, "requests", user.get_requests())

		def search_func(text):
			user.server.search_people(text)

		self.people = Block(SearchedPerson, "global search", [], search_func)

		self.addWidget(self.contacts)
		self.addWidget(self.requests)
		self.addWidget(self.people)
		self.show_contacts()

	def show_contacts(self):
		self.contacts.showIt()
		self.requests.setVisible(False)
		self.people.setVisible(False)

	def show_requests(self):
		self.contacts.setVisible(False)
		self.requests.showIt()
		self.people.setVisible(False)

	def show_people(self):
		self.contacts.setVisible(False)
		self.requests.setVisible(False)
		self.people.showIt()



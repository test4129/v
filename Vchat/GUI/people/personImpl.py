from .person import Person
from PySide.QtGui import QLabel
from ..guiStates import GUIStates
from ..buttons import AddButton
from superdata.us3r import user


class Contact(Person):
	def __init__(self, person):
		super(Contact, self).__init__(person[1])
		online = person[2][1] != -1
		self.uid = person[0]
		self.status = QLabel("online" if online else "offline")
		self.status.setObjectName("StatusOn" if online else "StatusOff")
		self.status.setFixedHeight(16)
		self.left.addWidget(self.status)

	def mousePressEvent(self, *args, **kwargs):
		GUIStates.set_contact_id(self.uid)


class SearchedPerson(Person):
	def __init__(self, person):
		super(SearchedPerson, self).__init__(person[1])

		def send_req():
			user.server.send_request(person[0])

		self.sendButton = AddButton(send_req)
		self.main.addWidget(self.sendButton)


class Request(Person):
	def __init__(self, person):
		super(Request, self).__init__(person[1])

		def accept_req():
			user.server.add_to_contacts(person[0])

		self.acceptButton = AddButton(accept_req)
		self.main.addWidget(self.acceptButton)

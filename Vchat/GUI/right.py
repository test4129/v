from PySide.QtGui import QFrame, QLabel
from PySide.QtCore import Qt
from .canvasGL import CanvasGL
from .inCalls import InCallsLayout
from .buttons import WhiteButton
from .buttonsImpl import RemoveButton, StopButton, CallButton


class RightContainer(QFrame):
	def __init__(self, main_window):
		super(RightContainer, self).__init__()
		self.setObjectName("Right")

		b_contacts = WhiteButton(main_window.left.show_contacts)
		b_contacts.setParent(self)
		b_contacts.move(10, 10)
		b_reqs = WhiteButton(main_window.left.show_requests)
		b_reqs.setParent(self)
		b_reqs.move(60, 10)
		b_search = WhiteButton(main_window.left.show_people)
		b_search.setParent(self)
		b_search.move(110, 10)

		self.canvas = CanvasGL()
		self.canvas.setParent(self)
		self.canvas.move(40, 60)
		self.canvas.setFixedSize(360, 450)
		self.canvas.setVisible(True)

		self.in_calls_layout = InCallsLayout()
		self.in_calls_layout.setParent(self)
		self.in_calls_layout.move(400, 20)
		self.in_calls_layout.setFixedSize(100, 300)
		self.in_calls_layout.setObjectName("Left")

		self.call_button = CallButton()
		self.call_button.setParent(self)
		self.call_button.move(200, 550)
		self.call_button.setVisible(False)
		self.stop_button = StopButton()
		self.stop_button.setParent(self)
		self.stop_button.move(200, 550)
		self.stop_button.setVisible(False)
		self.login_label = QLabel()
		self.login_label.setObjectName("LoginLabel")
		self.login_label.setParent(self)
		self.login_label.move(175, 530)
		self.login_label.setFixedSize(100, 20)
		self.login_label.setAlignment(Qt.AlignCenter)
		self.login_label.setVisible(False)
		self.remove_button = RemoveButton()
		self.remove_button.setParent(self)
		self.remove_button.move(400, 500)
		self.remove_button.setVisible(False)

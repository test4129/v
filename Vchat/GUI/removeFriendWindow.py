from PySide.QtGui import QMainWindow, QPushButton, QFrame, QGridLayout, QLabel
from superdata.us3r import user


class RemoveButton(QPushButton):
	def __init__(self, uid, window):
		super(RemoveButton, self).__init__("Yes")
		self.uid = uid
		self.parentWindow = window

	def mousePressEvent(self, *args, **kwargs):
		user.server.remove_friend(self.uid)
		self.parentWindow.close()


class CancelButton(QPushButton):
	def __init__(self, window):
		super(CancelButton, self).__init__("No")
		self.parentWindow = window

	def mousePressEvent(self, *args, **kwargs):
		self.parentWindow.close()


class RemoveFriendWindow(QMainWindow):
	def __init__(self, uid):
		super(RemoveFriendWindow, self).__init__()
		self.uid = uid
		widget = QFrame()
		layout = QGridLayout()
		widget.setLayout(layout)
		self.setCentralWidget(widget)
		layout.addWidget(QLabel("Are you sure you want to delete " + user.get_contact(uid)[1] + " from your contacts"), 0, 0, 1, 2)
		layout.addWidget(RemoveButton(uid, self), 1, 0, 1, 1)
		layout.addWidget(CancelButton(self), 1, 1, 1, 1)

from superdata.us3r import user


class GUIStates:
	NO_CONTACT = -1

	contact_id = NO_CONTACT

	@staticmethod
	def set_widgets(call_button, stop_button, login_label, remove_button):
		GUIStates.call_button = call_button
		GUIStates.stop_button = stop_button
		GUIStates.login_label = login_label
		GUIStates.remove_button = remove_button

	@staticmethod
	def set_contact_id(uid):
		GUIStates.contact_id = uid
		if uid != GUIStates.NO_CONTACT:
			GUIStates.call_button.setVisible(True)
			GUIStates.remove_button.setVisible(True)
			GUIStates.login_label.setVisible(True)
			GUIStates.login_label.setText(user.get_contact(uid)[1])

	@staticmethod
	def get_contact_id():
		return GUIStates.contact_id

	@staticmethod
	def set_no_contact():
		GUIStates.contact_id = GUIStates.NO_CONTACT

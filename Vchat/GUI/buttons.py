from PySide.QtGui import QPushButton


class Button(QPushButton):
	def __init__(self, click_func):
		super(Button, self).__init__()
		self.setFixedSize(48, 48)
		self.onClick = click_func

	def onClick(self):
		pass

	def mousePressEvent(self, *args, **kwargs):
		self.onClick()


class AddButton(Button):
	def __init__(self, click_func):
		super(AddButton, self).__init__(click_func)
		self.setObjectName("AddButton")


class GreenButton(Button):
	def __init__(self, click_func):
		super(GreenButton, self).__init__(click_func)
		self.setObjectName("GreenButton")


class RedButton(Button):
	def __init__(self, click_func):
		super(RedButton, self).__init__(click_func)
		self.setObjectName("RedButton")


class WhiteButton(Button):
	def __init__(self, click_func):
		super(WhiteButton, self).__init__(click_func)
		self.setObjectName("WhiteButton")


class RemoveButtonAbstract(Button):
	def __init__(self, click_func):
		super(RemoveButtonAbstract, self).__init__(click_func)
		self.setObjectName("RemoveButton")

from .buttons import GreenButton, RedButton, RemoveButtonAbstract
from .removeFriendWindow import RemoveFriendWindow
from .guiStates import GUIStates
from superdata.us3r import user


class CallButtonIn(GreenButton):
	def __init__(self, uid, parent_box):
		def call_func():
			user.call(uid)
			user.main_window.right.canvas.showIt()
			parent_box.setParent(None)

		super(CallButtonIn, self).__init__(call_func)


class RefuseButtonIn(RedButton):
	def __init__(self, parent_box):
		def refuse_func():
			user.refuse()
			parent_box.setParent(None)

		super(RefuseButtonIn, self).__init__(refuse_func)


class StopButton(RedButton):
	def __init__(self):
		def stop_call_func():
			user.stop_call()
			user.main_window.right.canvas.hideIt()
			GUIStates.set_contact_id(GUIStates.get_contact_id())
			self.setVisible(False)

		super(StopButton, self).__init__(stop_call_func)


class CallButton(GreenButton):
	def __init__(self):
		def call_func():
			user.call(GUIStates.get_contact_id())
			user.main_window.right.canvas.showIt()
			user.main_window.right.remove_button.setVisible(False)

		super(CallButton, self).__init__(call_func)


class RemoveButton(RemoveButtonAbstract):
	def __init__(self):
		def remove_friend_func():
			uid = GUIStates.get_contact_id()
			if uid != -1 and user.get_contact(uid):
				self.removeWin = RemoveFriendWindow(uid)
				self.removeWin.show()

		super(RemoveButton, self).__init__(remove_friend_func)

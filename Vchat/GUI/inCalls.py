from PySide.QtGui import QVBoxLayout, QHBoxLayout, QLabel, QWidget
from .buttonsImpl import RefuseButtonIn, CallButtonIn
from superdata.us3r import user


class InCall(QHBoxLayout):
	def __init__(self, uid):
		super(InCall, self).__init__()
		self.addWidget(RefuseButtonIn(self))
		center_box = QHBoxLayout()
		person = user.get_contact(uid)
		label_name = QLabel(person[1])
		label_name.setObjectName("IncomingCallUsername")
		center_box.addWidget(label_name)
		label_incoming_call_text = QLabel("Incoming call")
		label_incoming_call_text.setObjectName("IncomingCallText")
		center_box.addWidget(label_incoming_call_text)
		self.addLayout(center_box)
		self.addWidget(CallButtonIn(uid, self))


class InCallsLayout(QWidget):
	def __init__(self):
		super(InCallsLayout, self).__init__()
		self.main = QVBoxLayout()

	def event(self, event):
		if event.type() == 555:
			self.main.addLayout(InCall(event.uid))
			print("MAIN ADDED INCALL()")
		else:
			return super(InCallsLayout, self).event(event)

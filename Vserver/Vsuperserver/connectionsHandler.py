import asyncore
import socket
import settings
from utils.parsejson import bjson2object, object2bjson
from json import JSONDecodeError
from handlers.userHandler import Handler
from db import DB
from usersHandlersPool import UsersHandlersPool
from random import getrandbits


TOKEN_BITS = 128
BUSY_USER = -2
INVALID_USER = -1


class ConnectionsHandler(asyncore.dispatcher):
	MAX_PEERS = 8192

	def __init__(self):
		asyncore.dispatcher.__init__(self)
		self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
		self.bind((settings.address, settings.mainPort))
		self.listen(ConnectionsHandler.MAX_PEERS)
		self.first = True

		self.users = DB.Users()

	@staticmethod
	def make_a_token():
		return hex(getrandbits(TOKEN_BITS))[2:].ljust(TOKEN_BITS // 8, "0")

	def handle_accept(self):
		connection = self.accept()
		if connection is not None:
			try:
				sock, addr = connection
				sock.setblocking(True)
				sock.settimeout(3.0)  # ????
				try:
					req = bjson2object(sock.recv(512))
					result = self.validate_user(req["data"])
					if result == INVALID_USER:
						sock.send(object2bjson({"result": "invalid"}))
						sock.close()
					elif result == BUSY_USER:
						sock.send(object2bjson({"result": "busy"}))
						sock.close()
					else:
						token = self.make_a_token()
						Handler(sock, result, token, port=req["data"]["port"], dbusers=self.users)
				except (JSONDecodeError, KeyError):
					pass
			except socket.error:
				pass


	def shutdown(self):
		print()
		self.users.close()

	def validate_user(self, data):
		user = self.users.get_by_login(data["login"])
		if user is None:
			return INVALID_USER
		if user.password != data["password"]:
			return INVALID_USER
		if UsersHandlersPool.exists(user.id):
			return BUSY_USER
		return user



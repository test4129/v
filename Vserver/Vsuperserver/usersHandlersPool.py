class UsersHandlersPool:
	units = {}

	@staticmethod
	def add(user_handler):
		UsersHandlersPool.units[user_handler.user.id] = user_handler

	@staticmethod
	def rem(uid):
		del UsersHandlersPool.units[uid]

	@staticmethod
	def get(uid):
		return UsersHandlersPool.units[uid]

	@staticmethod
	def exists(uid):
		return uid in UsersHandlersPool.units

from django.db import models
from django.utils import timezone
from hashlib import sha256


class VUser(models.Model):
	_id = models.IntegerField(primary_key=True, auto_created=True)
	login = models.CharField(max_length=20)
	reg_date = models.DateField(default=timezone.now)
	email = models.EmailField(blank=True)
	password = models.CharField(max_length=64)
	avatar = models.FilePathField(blank=True)
	contacts = models.ManyToManyField("VUser", blank=True)
	requests = models.ManyToManyField("VUser", blank=True, related_name="reqs")

	@staticmethod
	def makenew(login, password):
		return VUser.objects.create(login=login, password=sha256(password.encode()).hexdigest())
